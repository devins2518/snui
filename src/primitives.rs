use std::ops::{Deref, DerefMut};

#[derive(Copy, Clone, Debug)]
pub enum Direction {
    Left,
    Right,
    Up,
    Down,
}

#[derive(Copy, Clone, Debug)]
pub enum Orientation {
    Vertical,
    Horizontal,
}

#[derive(Copy, Clone, Debug)]
pub enum Content {
    Empty,
    Number(i32),
    Char(char),
}

#[derive(Clone, Debug)]
pub enum Error {
    Null,
    Unavailable,
    Message(&'static str),
}

// API to utilize the Canvas
pub trait Canvas<'c, G: Geometry> {
    fn paint(&self);
    // Returns a matrix of Cells representing the current state of the Canvas
    fn get_area(&self) -> &Vec<Vec<Cell<'c, G>>>;
    // These 2 are drawing APIs
    fn anchor<D: Geometry + Drawable<'c, G>>(&'c mut self, object: &'c D, anchor: Direction);
    fn set(&mut self, x: usize, y: usize, cell: Cell<'c, G>);
}

pub trait Container<'c, G: Geometry> {
    fn len(&self) -> usize;
    // Appends an object at the end of a Container
    fn add(&mut self, object: &'c G) -> Result<(), Error>;
    fn get_child(&self) -> Vec<G>;
}

pub trait Geometry {
    fn get_width(&self) -> usize;
    fn get_height(&self) -> usize;
    fn get_position(&self) -> Option<Point>;
    fn set_position(&mut self, pos: Point);
}

pub trait Drawable<'c, G: Geometry> {
    fn draw(&'c self, width: usize, height: usize) -> Surface<'c, G>;
    fn damage<C: Canvas<'c, G>>(&self, canvas: &'c mut C);
}

pub trait Transform {
    fn scale(&mut self, f: u32);
}

#[derive(Copy, Clone, Debug)]
pub struct Point {
    x: usize,
    y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Self {
        Point { x, y }
    }
}

// Methods for the vector type
#[derive(Copy, Clone, Debug)]
pub struct Vector {
    step: i32,
    direction: Direction,
}

impl Vector {
    pub fn new() -> Self {
        // TO-D0
        Vector {
            direction: Direction::Right,
            step: 1,
        }
    }
    pub fn get_step(&self) -> i32 {
        self.step
    }
    pub fn get_direction(&self) -> Direction {
        self.direction
    }
    // Methods for unitary vectors
    pub fn to(direction: Direction) -> Vector {
        Vector { step: 1, direction }
    }
    pub fn left(step: i32) -> Vector {
        Vector {
            step,
            direction: Direction::Left,
        }
    }
    pub fn right(step: i32) -> Vector {
        Vector {
            step,
            direction: Direction::Right,
        }
    }
    pub fn up(step: i32) -> Vector {
        Vector {
            step,
            direction: Direction::Up,
        }
    }
    pub fn down(step: i32) -> Vector {
        Vector {
            step,
            direction: Direction::Down,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Cell<'c, G: Geometry> {
    parent: Option<&'c G>,
    content: Content,
}

impl<'c, G: Geometry + Copy> Cell<'c, G> {
    fn new(parent: &'c G, content: Content) -> Cell<'c, G> {
        Cell {
            parent: Some(parent),
            content,
        }
    }
}

impl<'c, G: Geometry> Cell<'c, G> {
    fn empty() -> Cell<'c, G> {
        Cell {
            parent: None,
            content: Content::Empty,
        }
    }
}

// A minimal implementation of a canvas objects can use to draw themselves
pub struct Surface<'c, G: Geometry> {
    width: usize,
    height: usize,
    canvas: Vec<Vec<Cell<'c, G>>>,
}

impl<'c, G: Geometry + Copy> Canvas<'c, G> for Surface<'c, G> {
    fn paint(&self) {}
    fn get_area(&self) -> &Vec<Vec<Cell<'c, G>>> {
        &self.canvas
    }
    fn anchor<D: Geometry + Drawable<'c, G>>(&'c mut self, object: &'c D, anchor: Direction) {
        let width = if self.width < object.get_width() {
            self.width
        } else {
            object.get_width()
        };
        let height = if self.height < object.get_height() {
            self.height
        } else {
            object.get_height()
        };
        let buf = object.draw(width, height);
        let buf_area = buf.get_area();
        let mut origin = Point::new((self.width - width) / 2, (self.height - height) / 2);
        match anchor {
            Direction::Left => origin.x = 0,
            Direction::Right => origin.x = self.width - width,
            Direction::Up => origin.y = 0,
            Direction::Down => origin.y = self.height - height,
        }
        for y in origin.y..width {
            for x in origin.x..width {
                self.canvas[y][x] = buf_area[y][x];
            }
        }
    }
    fn set(&mut self, x: usize, y: usize, cell: Cell<'c, G>) {
        let y = self.height - y;
        self.canvas[y][x] = cell;
    }
}

impl<'c, G: Geometry + Clone> Surface<'c, G> {
    fn new(width: usize, height: usize) -> Surface<'c, G> {
        let canvas = vec![vec![Cell::empty(); width]; height];
        Surface {
            width,
            height,
            canvas,
        }
    }
    fn fill(width: usize, height: usize, cell: Cell<'c, G>) -> Surface<'c, G> {
        let canvas = vec![vec![cell; width]; height];
        Surface {
            width,
            height,
            canvas,
        }
    }
}

// Rectangle struct - Geometry + Drawable (soon)
#[derive(Copy, Clone, Debug)]
pub struct Rectangle {
    width: usize,
    height: usize,
    pub empty: bool,
    pos: Option<Point>,
}

impl<'c> Drawable<'c, Self> for Rectangle {
    fn draw(&'c self, width: usize, height: usize) -> Surface<'c, Self> {
        let cell = Cell::new(self, Content::Char('.'));
        // let cell = Cell::empty();
        // Ajouter la version pour empty plus tard
        Surface::fill(self.width, self.height, cell)
    }
    fn damage<C: Canvas<'c, Self>>(&self, canvas: &'c mut C) {}
}

impl Geometry for Rectangle {
    fn set_position(&mut self, pos: Point) {
        self.pos = Some(pos);
    }
    fn get_position(&self) -> Option<Point> {
        self.pos
    }
    fn get_width(&self) -> usize {
        self.width
    }
    fn get_height(&self) -> usize {
        self.height
    }
}

impl Rectangle {
    fn new(width: usize, height: usize) -> Rectangle {
        Rectangle {
            width,
            height,
            pos: None,
            empty: true,
        }
    }
}

// Line struct - Geometry + Drawable (soon)
#[derive(Copy, Clone, Debug)]
pub struct Line {
    len: usize,
    pos: Option<Point>,
    orientation: Orientation,
}

impl<'c> Drawable<'c, Self> for Line {
    fn draw(&'c self, width: usize, height: usize) -> Surface<'c, Self> {
        let cell = Cell::new(self, Content::Char('.'));
        // let cell = Cell::empty();
        match self.orientation {
            Orientation::Horizontal => Surface::fill(self.len, 1, cell),
            Orientation::Vertical => Surface::fill(1, self.len, cell),
        }
    }
    fn damage<C: Canvas<'c, Self>>(&self, canvas: &'c mut C) {}
}

impl Geometry for Line {
    fn set_position(&mut self, pos: Point) {
        self.pos = Some(pos);
    }
    fn get_position(&self) -> Option<Point> {
        self.pos
    }
    fn get_width(&self) -> usize {
        match self.orientation {
            Orientation::Vertical => 0,
            Orientation::Horizontal => self.len,
        }
    }
    fn get_height(&self) -> usize {
        match self.orientation {
            Orientation::Horizontal => 0,
            Orientation::Vertical => self.len,
        }
    }
}

impl Line {
    fn new(orientation: Orientation, len: usize) -> Line {
        Line {
            len,
            orientation,
            pos: None,
        }
    }
}

// Node struct - Container + Geometry + Drawable
#[derive(Clone, Debug)]
pub struct Node<'c, G: Geometry> {
    head: G,
    pos: Option<Point>,
    parent: Option<&'c Self>,
    tail: Option<Box<Self>>,
}

impl<'c, G: Geometry + Clone> Geometry for Node<'c, G> {
    fn set_position(&mut self, pos: Point) {
        self.pos = Some(pos);
    }
    fn get_position(&self) -> Option<Point> {
        self.pos
    }
    fn get_width(&self) -> usize {
        self.head.get_width()
            + (if let Some(tail) = &self.tail {
                tail.get_width()
            } else {
                0
            })
    }
    fn get_height(&self) -> usize {
        self.head.get_height()
            + (if let Some(tail) = &self.tail {
                tail.get_height()
            } else {
                0
            })
    }
}

impl<'c, G: Geometry + Copy> Container<'c, G> for Node<'c, G> {
    fn len(&self) -> usize {
        1 + if let Some(tail) = &self.tail {
            tail.deref().len()
        } else {
            0
        }
    }
    // Appends an object at the end of a Container
    fn add(&mut self, object: &'c G) -> Result<(), Error> {
        if let Some(tail) = &mut self.tail {
            tail.deref_mut().add(object)
        } else {
            self.tail = Some(Box::new(Node::new(*object)));
            Ok(())
        }
    }
    // Returns the list of child windows
    fn get_child(&self) -> Vec<G> {
        let mut v = Vec::new();
        v.push(self.head);
        if let Some(tail) = &self.tail {
            let mut t = tail.as_ref().get_child();
            v.append(&mut t);
        }
        v
    }
}

impl<'c, G: Geometry> Node<'c, G> {
    pub fn new(head: G) -> Node<'c, G> {
        Node {
            head,
            pos: None,
            tail: None,
            parent: None,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Mobile<'c, G: Geometry + Copy> {
    object: &'c G,
    vec: Vector,
}

impl<'c, G: Geometry + Copy> Transform for Mobile<'c, G> {
    fn scale(&mut self, f: u32) {
        let step = self.vec.get_step();
        let direction = self.vec.get_direction();
        self.vec = Vector {
            direction,
            step: step * (f as i32),
        };
    }
}

impl<'c, G: Geometry + Copy> Mobile<'c, G> {
    fn new(object: &'c G, vec: Vector) -> Self {
        Mobile { vec, object }
    }
    fn get_object(&self) -> G {
        *self.object
    }
}

// Composer struct - Container + Geometry + Drawable
// Represents complex object structures
#[derive(Clone, Debug)]
pub struct Composer<'c, G: Geometry + Copy> {
    pos: Option<Point>,
    childs: Vec<Mobile<'c, G>>,
}

impl<'c, G: Geometry + Copy> Container<'c, G> for Composer<'c, G> {
    fn len(&self) -> usize {
        self.childs.len()
    }
    fn add(&mut self, object: &'c G) -> Result<(), Error> {
        self.childs.push(Mobile::new(object, Vector::up(0)));
        Ok(())
    }
    fn get_child(&self) -> Vec<G> {
        let childs = self.childs.iter().map(|mobile| mobile.get_object());
        childs.collect()
    }
}

impl<'c, G: Geometry + Drawable<'c, G> + Copy + Transform> Transform for Composer<'c, G> {
    fn scale(&mut self, f: u32) {
        for mobile in &mut self.childs {
            mobile.scale(f);
        }
    }
}
